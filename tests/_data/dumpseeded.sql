CREATE DATABASE  IF NOT EXISTS `courseadminfinal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `courseadminfinal`;
-- MySQL dump 10.13  Distrib 5.6.27, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: courseadminfinal
-- ------------------------------------------------------
-- Server version	5.6.27-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course_module`
--

DROP TABLE IF EXISTS `course_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_module` (
  `course_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`module_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_module`
--

LOCK TABLES `course_module` WRITE;
/*!40000 ALTER TABLE `course_module` DISABLE KEYS */;
INSERT INTO `course_module` VALUES (2,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,10,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `course_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Computing','BIS20705549',10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Software & Systems','BIS37501494',8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Web','BIS4403159',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Application Development','BIS28625001',3,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_module`
--

DROP TABLE IF EXISTS `item_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_module` (
  `item_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `complete` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`module_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_module`
--

LOCK TABLES `item_module` WRITE;
/*!40000 ALTER TABLE `item_module` DISABLE KEYS */;
INSERT INTO `item_module` VALUES (1,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,11,0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `item_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Id vero accusamus illo doloribus porro quo.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Non voluptates officiis provident quam dolorem nesciunt.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Quos laborum temporibus fugiat illo laborum omnis eius.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Eligendi asperiores iste non est cumque quia beatae.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Animi praesentium dolorum sint.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Dolorem omnis eveniet mollitia sed maiores ipsam.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Quis repellat architecto est dicta non aperiam modi quaerat.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Culpa qui quia laboriosam ut aut eos.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Sed officia cumque sapiente.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Sunt dicta maxime ullam a placeat aliquid facere.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'Cupiditate doloremque error eum quis magnam dignissimos ea maiores.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'Eum nulla et incidunt quibusdam id distinctio nisi quidem.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'Rem fugit quia sit voluptas expedita quae.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'Et et voluptas doloribus voluptatum.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'Voluptas nesciunt ea est esse officia.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'Quibusdam consequatur dignissimos aut in earum nihil.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,'Architecto voluptatem dolore aut sed impedit tenetur.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,'Odit fuga error sequi iusto.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,'Aut consequatur illum sit.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'Et impedit et vel quia quis distinctio cupiditate et.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'Eos deleniti id beatae.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'Qui asperiores repudiandae debitis beatae.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,'Nihil harum dolorem deleniti et illo.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'Accusantium iure dolores enim nobis.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,'Nihil voluptas explicabo quo.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,'Illum mollitia excepturi consequatur quia velit.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,'Nihil non ut exercitationem.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,'Reiciendis ut quas ducimus esse.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,'Fuga eos laudantium maxime non.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,'Quos consectetur debitis dolores culpa nobis quis nulla.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,'Nisi facilis odio voluptatem nulla.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,'At tempore omnis sit consectetur consequatur rerum totam.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,'Magni quia qui aut labore consectetur aliquid eius.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,'Quia quia vitae dolor hic minima voluptatum deleniti.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,'Quasi deleniti dicta autem corrupti exercitationem.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'Ratione nihil ipsum vitae quia libero minima eum.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,'Animi ipsa qui quae at est.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,'Nobis laborum voluptas eos voluptas earum perferendis et et.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,'Aut assumenda id aut reprehenderit laborum est sed.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,'Et aut officia consequatur rerum nihil eius repellat.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_07_21_110723_create_courses_table',1),('2015_07_21_110733_create_modules_table',1),('2015_07_21_110813_create_items_table',1),('2015_07_21_110847_create_itemmodule_table',1),('2015_07_21_110855_create_coursemodule_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `modules_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'Module 1','CIS1693',8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Module 2','CIS4571',10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Module 3','CIS1761',10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Module 4','CIS2748',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Module 5','CIS2207',6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Module 6','CIS4820',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Module 7','CIS3983',7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Module 8','CIS1205',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Module 9','CIS1886',6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Module 10','CIS1586',8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'Test Module','CISTESTING',13,'2015-11-25 19:25:19','2015-11-25 19:25:19');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Scottie Greenfelder','dLeannon@Dibbert.com','v6OvY0fuHC','SHZYDsmNPc','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(2,'Ms. Fiona Gleason I','rHudson@yahoo.com','i9UZHxLcKS','ZC6W1e3NCS','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(3,'Mr. Fernando Parisian','Schuster.Agustina@gmail.com','pB2ghxF194','YHAXEKuhqx','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(4,'Natasha Rempel','fRogahn@gmail.com','efpU0BaB37','2MJVF9B4Pk','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(5,'Prof. Jocelyn Hartmann','Javonte38@yahoo.com','cGLY5Z2aZ1','5M3AwtH7Lr','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(6,'Katharina Willms I','Belle.Schiller@yahoo.com','xf7r3PwXof','9kLjUzLhLf','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(7,'Andreanne Keebler','Roob.Constantin@hotmail.com','KLv5quNwkN','nl1UraXTmV','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(8,'Glennie Kautzer','bLittle@Gottlieb.com','DhcEnpbcKi','XvMteuKpoq','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(9,'Reba Lebsack','Stark.Luella@McGlynn.org','tGwftXNckr','vPTwCTRXVL','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(10,'Dr. Gerson Brakus','Edward58@Wunsch.com','HDBOmK59P1','uM2PxptWID','2015-11-25 19:23:45','2015-11-25 19:23:45',0),(11,'admin','admin@example.com','$2y$10$8QdLJUwHFWAqfcQChJyWoOnX0KsHWKqaC1E0dEfI8Hlsi0pi4xpGe','VeOjLKDEWxVqZsZtUBVbHid23ZjKT06xDc3xCQvCmWG9sU7VZfcrPBlrTWoH','0000-00-00 00:00:00','2015-11-25 19:25:22',1),(12,'notadmin','notadmin@example.com','$2y$10$5CQxllxdErG3Mnh7LM9Iput06mMi4JoH2RwYgTK.sHyVzXeRSxgQa',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(13,'moduleleader','moduleleader@example.com','$2y$10$GQYH2GWH5eaf.WD7DnzxJuswdorwNQAwAshImCVWybOrxsU0A61ei','vEmfudEH21aFO1Ld6J1omUOs8qsaLh0ZGunDQbvvT3jPBC5OdkUO355zt5yz','0000-00-00 00:00:00','2015-11-25 19:24:25',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-25 19:26:54
