<?php
$I = new FunctionalTester($scenario);
$I->am('module leader');
$I->wantTo('mark items and complete or incomplete');

//Login as the moduleleader
//Note: potential issue with Codeception not registering $request->input('complete') value from ItemModule controller update method.
//Running the test with update('complete' => '1'), the code below that is commented out successfully passes.
//When
Auth::loginUsingId(13);
$I->seeAuthentication();
//When
$I->amOnPage('/admin/dash');
//And
$I->click('My Modules');
//Then
$I->canSeeCurrentUrlEquals('/admin/modules');
$I->see('Test Module', 'a');
$I->see('View module progress', 'a');
//Then
$I->click('View module progress');
//And
$I->canSeeCurrentUrlEquals('/admin/modules/11');
$I->see('Test Module Progress', 'h2');
$I->see('21markcomplete');
$I->seeInDatabase('item_module', [
    'item_id' => '21',
    'complete' => '0',
    'module_id' => '11'

]);
$I->seeElement('button', ['name' => '21markcomplete']);
$I->see('Eos deleniti id beatae.');
$I->dontSee('21markuncomplete');

//WHen
$I->click('button', ['name' => '21markcomplete'] );
//Then
$I->see('Item updated');
//Then
$I->amOnPage('/admin/modules/11');
//ANd
//$I->seeRecord('item_module', [
//  'item_id' => '21',
//  'complete' => '1',
//  'module_id' => '11'
//
//]);

//$I->seeElement('button', ['name' => '21markuncomplete']);

//And
//$I->dontSee('21hiddencomplete');

