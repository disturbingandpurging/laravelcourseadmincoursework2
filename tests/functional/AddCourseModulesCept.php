<?php
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('add a module to a course');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then
$I->amOnPage('/admin/courses/2/edit');
// And
$I->see('Software & Systems', 'h1');
$I->see('Module 1');
$I->see('Module 2');
$I->see('Module 7');
$I->see('Module 8');
$I->see('Module 9');
$I->see('Module 10');
// Then
$I->checkOption('form input[value="5"]');
// And
$I->click('Update Course');
// Then
$I->amOnPage('/admin/courses/2');
// And
// had to remove below line as flash messages are not viewable outside of the browser.
//$I->see('Your course has been updated!');
$I->see('Module 5');