<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('delete an item from a module and to show confirmation dialog');


// Log in as module leader
Auth::loginUsingId(13);
$I->seeAuthentication();

//When
$I->amOnPage('/admin/dash');
//And
$I->click('My Modules');
//Then
$I->seeCurrentUrlEquals('/admin/modules');
$I->see('View module progress', 'a');

//When
$I->click('View module progress');
//Then
$I->seeCurrentUrlEquals('/admin/modules/11');
//And
$I->see('Fuga eos laudantium maxime non.');

//when
$I->click('delete29');

//Then
$I->seeCurrentUrlEquals('/admin/items');
$I->dontSee('Fuga eos laudantium maxime non.');