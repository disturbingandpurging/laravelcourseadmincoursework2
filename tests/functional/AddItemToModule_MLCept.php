<?php 
$I = new FunctionalTester($scenario);
$I->am('Module Leader');
$I->wantTo('add an item to a module');

//Log in as ordinary (notadmin) user:
Auth::loginUsingId(13);
//potentially create a new record so that this teSt can be run?


//When
$I->amOnPage('/admin/dash');
//then
$I->see('My Courses', 'a');
$I->see('My Modules', 'a');
$I->see('My Items', 'a');
//WHen
$I->click('My Modules');
//Then
$I->amOnPage('/admin/modules');
//$I->haveInDatabase('modules', [
//    'title' => 'Module 13',
//    'leader' => '12'
//
//]);
$I->see('Modules', 'h1');
//and
$I->see('Test Module', 'a');
//other i's here too

//When
$I->click('Test Module');
//Then
$I->canSeeCurrentUrlEquals('/admin/modules/11');
//And
$I->see('Test Module', 'h1');
$I->see('Add Item', 'a');

//When
$I->click('Add Item');
//THen
$I->canSeeCurrentUrlEquals('/admin/items/create?11');
//And

$I->fillField('text', 'this is a new item');

//Then
$I->click('Create Item');

//CHeck that the user has been redirected?
$I->seeCurrentUrlEquals('/admin/items');
