<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('see the list of modules');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then Create some courses...
$I->haveRecord('modules', [
    'id' => 12,
    'title' => 'Web Design',
    'code' => 'CIS9911',
    'leader' => 11,
]);
$I->haveRecord('modules', [
    'id' => 13,
    'title' => 'OOP',
    'code' => 'CIS9912',
    'leader' => 8,
]);
// Then
$I->seeRecord('modules', ['title' => 'Web Design']);
$I->amOnPage('/admin/modules');
$I->see('Modules', 'h1');
$I->see('Web Design', 'a.item');
$I->see('OOP', 'a.item');
$I->click('Web Design');
$I->seeCurrentUrlEquals('/admin/modules/12');
$I->see('Web Design', 'h1');