<?php 
$I = new FunctionalTester($scenario);
$I->am('module leader');
$I->wantTo('view all module\'s progressions');

//Login as the moduleleader
//When
Auth::loginUsingId(13);
$I->seeAuthentication();
$I->amOnPage('/admin/dash');
$I->click('My Modules');
//Then
$I->canSeeCurrentUrlEquals('/admin/modules');
//And
$I->see('Test Module', 'a');
$I->see('0 out of 21 items completed', 'p');



