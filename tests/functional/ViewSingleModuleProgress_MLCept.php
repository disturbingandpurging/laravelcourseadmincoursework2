<?php
$I = new FunctionalTester($scenario);
$I->am('module leader');
$I->wantTo('view all module\'s progressions');

//Login as the moduleleader
//When
Auth::loginUsingId(13);
$I->seeAuthentication();
//And
$I->amOnPage('/admin/dash');
$I->click('My Modules');
//Then
$I->canSeeCurrentUrlEquals('/admin/modules');
$I->see('Test Module', 'a');
$I->see('View module progress', 'a');
//And
$I->click('View module progress');
//Then
$I->canSeeCurrentUrlEquals('/admin/modules/11');
$I->see('Test Module Progress', 'h2');
//and
$I->see('Overall Completion', 'h3');
$I->see('0%', 'p');
$I->see('Completed Items', 'h3');
$I->see('0/21', 'p');
$I->see('Default Items', 'h3');
$I->see('5', 'p');
