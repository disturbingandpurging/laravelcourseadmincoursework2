<?php
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('add a item to a module');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then
$I->amOnPage('/admin/modules/2');
// And
$I->see('module 2', 'h1');
$I->see('Animi praesentium dolorum sint.');
$I->dontSee('DEligendi asperiores iste non est cumque quia beatae.');
// Then
$I->click('Edit Module');
// And
$I->amOnPage('/admin/modules/2/edit');
// Then
$I->checkOption('form input[value="6"]');
// And
$I->click('Update Module');
// Then
$I->amOnPage('/admin/modules/2');
// And
$I->see('Eligendi asperiores iste non est cumque quia beatae.');