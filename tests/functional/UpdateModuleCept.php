<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('update a module');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then Create a new course...
$I->haveRecord('modules', [
    'id' => 13,
    'title' => 'Web Design',
    'code' => 'CIS9999',
    'leader' => 13,
]);
// When
$I->amOnPage('/admin/modules/13');
// Then
$I->click('Edit Module');
//Then
$I->amOnPage('/admin/modules/13/edit');
$I->see('Edit - Web Design', 'h1');
// And
$I->amGoingTo('Clear the code field in order to submit an invalid form');
// When
$I->fillField('code', null);
$I->click('Update Module');

// the above could and probably should be expanded to run a separate test on each form filed.

// Then
$I->expectTo('See the form again with the errors');
$I->seeCurrentUrlEquals('/admin/modules/13/edit');
$I->see('The code field is required');
// Then
$I->fillField('code', 'CIS9999');
$I->selectOption('moduleleader', 13);
//  And
$I->click('Update Module');

// Then
$I->seeCurrentUrlEquals('/admin/modules/13');
$I->see('Web Design', 'h1');
$I->seeRecord('modules', [
    'id' => 13,
    'title' => 'Web Design',
    'code' => 'CIS9999',
    'leader' => 13,
]);