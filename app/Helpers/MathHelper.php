<?php

namespace App\Helpers;

class MathHelper {
	//This is the helper file for the calculate percentage operation used to calculate the item completion percentage.
	//Show % sign parameter is false by default

	public static function percentage($count, $total, $showSign = FALSE) {
		//if there are no completed items
		if ($count == 0) {
			//Return 0% so that the foundation progress bar has a valid value.
			return '0%';
		}
		//perform the percentage calculation and append the percentage sign if true, blank if false.
		return round(($count/$total) * 100) . (($showSign) ? '%' : '');

	}

}
