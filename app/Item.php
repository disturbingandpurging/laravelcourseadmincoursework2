<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';

    protected $fillable = ['text', 'default'];

    public function modules()
    {
        return $this->belongsToMany('App\Module')->withPivot('complete')->withTimestamps();
    }
}
