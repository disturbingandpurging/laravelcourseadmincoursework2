<?php

namespace App\Http\Controllers;

use App\Item;
use App\Module;
use App\ItemModule;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Carbon;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ItemModuleController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {

        /*$module_id = $request->get('module_id');

        $itemmodule = ItemModule::findORFail($id);

        $itemmodule->update([
            'completed' => $request->get('completed')
        ]);*/

        $GLOBALS ['module_id'] = $request->get('module_id');
        $GLOBALS ['item_id'] = $request->get('item_id');
        $module_id = $request->get('module_id');
        $mytime = Carbon\Carbon::now();
//        $itemmodule = ItemModule::findORFail($id);

        //If the user is module leader
        if (Auth::user()->admin == '2') {

            //query the itemmodule table (update) where the itemid and moduleid is equal to the request made, then update the complete status of the complete input in the show.blade template.

            $itemModule = DB::table('item_module')->where('module_id', $GLOBALS['module_id'])->where('item_id', $GLOBALS['item_id'])->update(['complete' => $request->input('complete'), 'updated_at' => $mytime]);
            return redirect()->to('/admin/modules/' . $module_id)->with('message', 'Item updated');
        }


    }

    public function destroy($id)
    {
        //
    }

}
