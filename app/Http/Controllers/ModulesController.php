<?php

namespace App\Http\Controllers;
use App\User;
use App\Module;
use App\ItemModule;
use App\Item;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;


class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // return a list of all modules if module leader
        if(Auth::user()->admin == '2') {
            //get the module with the items join where the user id is current to the logged in user's id.
            $allModules = Module::with('items')->where('leader', Auth::user()->id)->get();
        } else {
            //Else just return all items for all modules (admin)
            $allModules = Module::with('items')->get();

        }
        //retrieve all modules count and assign to total
        $total = $allModules->count();
        //Return all completed items (1)
        $completed = ItemModule::where('complete', '=', '1')->get();

        //Old method
//        return view('admin.modules.list', ['allModules' => $allModules, 'complete' => $completed, 'total' => '$total']);

        //New method - Use php's array compact to pass multiple variables to the view
        return view('admin.modules.list', compact('allModules', 'total', 'completed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // Return all module leaders
        $users = User::where('admin', '2')->get();

        $itemModuleAssociations = Module::itemModuleAssociations(0);

        return view('admin.modules.create', ['users' => $users])->with(['itemModuleAssociations' => $itemModuleAssociations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\CreateModulesRequest $request)
    {
        $module = new Module;

        $module->title = $request->title;
        $module->code = $request->code;
        $module->leader = $request->moduleleader;

        $module->save();

        $itemModules = $request->get('itemModules');

        // write module associations
        if (count($itemModules) > 0 ) {
            foreach ($itemModules as $item_id) {
                DB::table('item_module')->insert([
                    'module_id' => $module->id, 'item_id' => $item_id
                ]);
            }
        }

        return Redirect::route('admin.modules.show', [$module]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // return a single module based on module id
        $module = Module::findORFail($id);
        $moduleleader = User::find($module->leader);
        $allModules = Module::with('items')->where('leader', Auth::user()->id)->get();
        $total = $allModules->count();
        //Return all completed items (1)
        $completed = ItemModule::where('complete', '=', '1')->get();
        //Get all items marked as default
        $default = Item::where('default', '=', '1')->get();

        //Pass variables to the view.
        return view('admin.modules.show', ['module' => $module])->with(['moduleleader' => $moduleleader, 'total' => $total, 'completed' => $completed, 'allModules' => $allModules, 'default' => $default]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //Populate select box only with module leaders (admin status = 2)
        $users = User::where('admin', 2)
            ->orderBy('name', 'desc')
            ->get();
        $userselector = array();
        foreach($users as $user) {
            $userselector[$user->id] = $user->name;
        };
        $module = Module::findOrFail($id);
        // pass over two arrays/variables to the view.

        $itemModuleAssociations = Module::itemModuleAssociations($id);

        return view('admin.modules.edit', ['module' => $module])->with(['userselector' => $userselector])->with(['itemModuleAssociations' => $itemModuleAssociations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Requests\CreateModulesRequest $request, $id)
    {
        $module = Module::findORFail($id);

        $module->update([

            'title' => $request->get('title'),
            'code' => $request->get('code'),
            'leader' => $request->get('moduleleader')
        ]);


        $itemModuleAssociations = Module::itemModuleAssociations($id);
        $itemModules = $request->get('itemModules');

        foreach ($itemModuleAssociations as $item)
        {
            $skipped = false;
            // error capture for having no items associated
            if (count($itemModules) > 0 ) {
                // check if association already exists if it does remove it from the $courseModules array then remove it frm the db
                if ($item->checked == 1 && (($key = array_search($item->id, $itemModules)) !== false)) {
                    unset($itemModules[$key]);
                    $skipped = true;
                }
            }
            // delete any removed associations
            if($item->checked == 1 && !$skipped)
            {
                DB::table('item_module')->where('module_id', '=', $id)->where('item_id', '=', $item->id)->delete();
            }
            unset($key);
        }
        // write module associations
        if (count($itemModules) > 0 ) {
            foreach ($itemModules as $item_id) {
                DB::table('item_module')->insert([
                    'module_id' => $id, 'item_id' => $item_id
                ]);
            }
        }

        return Redirect::route('admin.modules.show', $module->id)->with('message', 'Your module has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        Module::destroy($id);

        // redirect
        //Session::flash('message', 'Successfully deleted the Module!');
        return \Redirect::route('admin.modules.index');
    }



}
