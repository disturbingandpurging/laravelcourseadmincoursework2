<?php

namespace App\Http\Controllers;

use Auth;
use App\Item;
use App\ItemModule;
use App\Module;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // return a list of all items in descending order
        $allItems = Item::all()->sortByDesc('id');
        return view('admin.items.list', ['allItems' => $allItems]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //If the user is module leader, set the select dropdown to contain only modules pertaining to that module leader's id.
        if(Auth::user()->admin == '2') {
            //return all modules
            $modules = Module::all();
            //Modules associated with leader 13 not showing for some reason on student server?
//            $modules = Module::all()->where('leader', '13');
        }

        return view('admin.items.create', compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\CreateItemsRequest $request)
    {
        $item = new Item;

        $item->text = $request->text;
        $item->default   = $request->default;

        $item->save();

        //if the admin status is equal to the module leader, then save the created item with completion set as 0.
        if (Auth::user()->admin == '2'){

            $itemmodule = new ItemModule;
            $itemmodule->module_id = $request->module_id;
            $itemmodule->complete = 0;
            $itemmodule->item_id = $item->id;

            $itemmodule->save();


        }

        return Redirect::route('admin.items.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);

        // pass over two arrays/variables to the view.
        return view('admin.items.edit', ['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Requests\CreateItemsRequest $request, $id)
    {
        $item = Item::findORFail($id);

        $item->update([
            'text' => $request->get('text'),
            'default' => $request->get('default')
        ]);

        return Redirect::route('admin.items.index')->with('message', 'Your item has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

      Item::destroy($id);

        // redirect
        return \Redirect::route('admin.items.index')->with('message', 'Your Item was deleted!');
    }


}
