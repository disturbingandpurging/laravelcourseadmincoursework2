@extends('layouts.master')

@section('title', 'Add a New Item')
@stop

@section('content')

    <h1 class="small-12 columns">Add new Item</h1>

    @if (count($errors) > 0)
        <div class="alert-box alert large-12 columns">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{url('admin/items') }}" enctype="multipart/form-data" id="form" data-abide>
        <!-- Item Text Form Input-->
        <div class="large-12 columns">
            <label for="text">Item Text:</label>
            <input type="text" id="text" name="text" placeholder="Item text" value="{{ Input::old('text') }}" required >
            <small class="error">item text is required.</small>
        </div>

        <div class="large-12 columns">
            <label for="default">Default Item:</label>
            <input type="checkbox" id="default" name="default" value="{{ Input::old('default') }}" checked >
        </div>

        <!--If the admin status is module leader...-->
        <?php $adminstatus = (Auth::user()->admin); ?>
        @if ($adminstatus == '2')

            <div class="large-12 columns">

                <?php
                //create an array to store select value items
                $select = [];

                foreach ($modules as $module) {
                    //for each module, populate the select item associated with the module id with the module title as the label
                    $select[$module->id] = $module->title;
                }

                ?>
                <!--create the select input-->
                {!! Form::select('module_id', $select) !!}
            </div>
            @endif
                    <!-- Item submit button -->
            <div class="large-12 columns">
                <button id="submit" name="submit" class="button right">Create Item</button>
            </div>
            {!! csrf_field() !!}
    </form>

@stop