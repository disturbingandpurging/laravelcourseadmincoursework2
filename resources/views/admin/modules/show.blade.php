@extends('layouts.master')
@section('title')
    Module - {{ $module->code }} - {{ $module->title }}
@stop
@section('content')

    @if ( Session::get('message'))
        <div class="alert-box warning large-12 columns">
            {{ Session::get('message') }}
            <a href="#" class="close">&times;</a>
        </div>
    @endif

    <h1 class="small-12 columns">{{ $module->title }}</h1>
    <div class="small-12 columns">
        <p> <strong>Module code:</strong> {{ $module->code }}</p>
        <p> <strong>Module Leader:</strong> {{ $moduleleader->name }}</p>

        <!--Module progress information if module leader-->
        @if(Auth::user()->admin =='2')
            <div class="panel text-center">
                <h2>{{$module->title}} Progress</h2>
                <p>Welcome to the {{$module->title}} progress page. Please feel free to observe module statistics and to add/remove items.</p>
                <div class="row">
                    <div class="large-4 columns">
                        <h3 class="subheader">Overall Completion</h3>
                        <!--retrieve completed item count associated with a module-->
                        <?php $total = $module->items->count();
                        $count = $completed->where('module_id', $module->id)->count()
                        ?>
                                <!--Insert completion percentage calculation-->
                        <p class="progress-text">{{Math::percentage($count, $total,  TRUE)}}</p>
                    </div>

                    <div class="large-4 columns">
                        <h3 class="subheader">Completed Items</h3>
                        <p class="progress-text">{{$count}}/{{$total}}</p>
                    </div>

                    <div class="large-4 columns">
                        <h3 class="subheader">Default Items</h3>
                        <p class="progress-text">{{$default->count()}}</p>
                    </div>
                </div>
            </div>
        @endif

        <h2>Associated Items</h2>
        @if (Auth::user()->admin == '2' )
            <a href="{{ route('admin.items.create', $module->id) }}" class="button small left success">Add Item</a>
        @endif
        @if ( !$module->items->count() )
            Your Module has no Items linked.
        @else
            <table class="large-12 columns">
                <thead>
                <tr>
                    <th>Item Description</th>
                    <th>Default</th>
                    @if(Auth::user()->admin =='2')
                        <th>Mark Complete/Incomplete</th>
                        <th>Delete Item</th>
                    @endif
                </tr>
                </thead>

                <tbody>
                @foreach( $module->items as $item )
                    <tr>
                        <td>
                            <p class="small-8 columns">{{ $item->text }}</p>
                            <!-- <form method="PUT" action="{{url('/admin/itemmodule') }}" enctype="multipart/form-data" id="form" data-abide> -->
                            {!! Form::model($item, array('method' => 'put', 'route' => ['admin.itemmodule.update', $item->id], 'data-abide' => '')) !!}
                            <input type="hidden" name="item_id" value="{{ $item->id }}" />
                            <input type="hidden" name="module_id" value="{{ $module->id }}" />
                            <!-- switch the buttons based on state of item -->
                        </td>

                        <td>
                            @if($item->default == '1')
                                <span class="secondary label">Default Item</span>

                            @endif

                        </td>

                        <td>
                            <!--If user is module leader...-->
                            @if(Auth::user()->admin =='2')
                            {{--If items are currently incompleted, show them,--}}
                            @if ($item->pivot->complete != '1')
                                    <!-- completed submit button -->

                            <button type="submit" name="{{$item->id}}markcomplete" class="button tiny">Mark as Complete</button>

                            <input type="hidden" name="complete" value="1" id="{{$item->id}}hiddencomplete" />
                            {{--Return all items that are marked as complete and give the user the option to render them incomplete again--}}
                            @else
                                    <!-- uncompleted submit button -->
                            <button type="submit" name="{{$item->id}}markuncomplete" class="button tiny warning" value="Mark as Incomplete">Mark as Incomplete</button>
                            <input type="hidden" name="complete" value="0" id="{{$item->id}}hiddenuncomplete"/>
                            @endif
                            @endif
                            {!! csrf_field() !!}
                            {!! Form::close() !!}
                        </td>


                        @if(Auth::user()->admin =='2')
                            <td>
                                <!--If module leader, then show delete item button-->
                                {!! Form::open(['url' => '/admin/items/' . $item->id, 'onSubmit' => 'return confirm("Are you sure you want to delete this?")']) !!}
                                {!! Form::hidden('_method', 'DELETE') !!}
                                {!! Form::submit('Delete this Item', array('class' => 'button tiny alert', 'name' =>  'delete' . $item->id)) !!}
                                {!! Form::close() !!}
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
                @endif
            </table>

            @if(Auth::user()->admin == '1')
                <a href="{{ route('admin.modules.edit', $module->id) }}" class="button small warning right">Edit Module</a>
            @endif
    </div>
@stop