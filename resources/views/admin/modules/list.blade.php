@extends('layouts.master')
@section('title')
    All Modules
@stop
@section('content')
    <?php $adminstatus = (Auth::user()->admin) ?>
    @if ($adminstatus == '1')
        <h1>Modules <a href="{{ url('admin/modules/create') }}" class="button right success small">
                Add a new module
            </a></h1>
    @else
        <h1>Modules</h1>
        @endif

        @if(Auth::check())
                <!--  The user is logged in... -->
        <div class="row small-12 columns">

            @if ($adminstatus == '1')
                    <!-- return a list of all courses -->
            <div class="small-12 columns">
                @foreach($allModules as $aModule)
                    <div class="row">
                        {!! Form::open(array('url' => '/admin/modules/' . $aModule->id)) !!}
                        <a href="{{ route('admin.modules.show', [$aModule->id]) }}" class="item">{{ $aModule->title }}</a>
                        {!! Form::hidden('_method', 'DELETE') !!}
                        {!! Form::submit('Delete this Module', array('class' => 'button tiny alert right', 'id' => $aModule->title . ' delete', 'name' => $aModule->title . ' delete')) !!}
                        {!! Form::close() !!}
                        <hr />
                    </div>
                @endforeach

            </div>
            @elseif($adminstatus == '2')

                    <!-- return only courses the staff are course leaders of and if none then return a message saying none to show. -->
            <?php $myModules = $allModules; ?>

            <table class="large-12 columns">
                <thead>
                <tr>
                    <th>Available Modules</th>
                    <th>Module progress</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($myModules as $myModule)
                    <tr>
                        <td>
                            <a href="{{ route('admin.modules.show', [$myModule->id]) }}" class="item">{{ $myModule->title }}</a>
                        </td>
                        <td>
                            <!--return completed items associated with the module id-->
                            <?php $count = $completed->where('module_id', $myModule->id)->count(); ?>
                                    <!--Echo out the completed items associated with module-->
                            <!--Populate foundation progress bar with calculated percentage from helper function, based on amount of completed items-->
                            <div class="progress">
                                <span class="meter" style="width: {{ Math::percentage($count, $myModule->items->count(), TRUE)  }};">
                                    <!--Percentage text within the span element-->
                                    <p class="percent">{{Math::percentage($count, $myModule->items->count(), TRUE)}}</p>
                                </span>
                            </div>
                            <p>{{ $count }} out of {{ $myModule->items->count()  }} items completed.</p>
                        </td>

                        <td>
                            <!--Link to individual module progress page-->
                            <a href="{{ route('admin.modules.show', [$myModule->id]) }}" class="button tiny right">View module progress</a>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>


            @endif
        </div>

    @endif



@stop