<footer class="footer large-12 columns">
    <div class="footerpad clearfix">
        <div class="small-12 medium-9 large-9 columns bottom" id="copyright">© Copyright 2015</div>
        <div class="small-12 medium-3 large-3 columns">
            <h4>Quick Links</h4>
            <ul class="footer-links">

                @if(Auth::check())
                    <li><a href="{{url('admin/dash')}}">Dashboard</a></li>
                    <li><a href="{{url('admin/courses')}}">Courses</a></li>
                    <li><a href="{{url('admin/modules')}}">Modules</a></li>
                    <li><a href="{{url('admin/items')}}">Items</a></li>
                @else
                    <li><a href="/">Home</a></li>
                @endif

                @if(Auth::check())
                    <li><a href="{{url('admin/logout')}}">Log Out</a></li>
                @else
                    <li><a href="{{url('admin/login')}}">Log In</a></li>
                @endif

            </ul>
        </div>
    </div>
</footer>

